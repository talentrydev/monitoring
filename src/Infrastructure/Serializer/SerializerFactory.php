<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Serializer;

use Talentry\Monitoring\Infrastructure\Serializer\Component\DecrementMetricSerializer;
use Talentry\Monitoring\Infrastructure\Serializer\Component\GaugeMetricSerializer;
use Talentry\Monitoring\Infrastructure\Serializer\Component\IncrementMetricSerializer;
use Talentry\Monitoring\Infrastructure\Serializer\Component\TimingMetricSerializer;

class SerializerFactory
{
    public function generate(): Serializer
    {
        $compositeSerializer = new CompositeSerializer();
        $compositeSerializer
            ->addSerializer(new DecrementMetricSerializer())
            ->addSerializer(new IncrementMetricSerializer())
            ->addSerializer(new GaugeMetricSerializer())
            ->addSerializer(new TimingMetricSerializer())
        ;

        return $compositeSerializer;
    }
}
