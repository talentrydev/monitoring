<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Infrastructure\Metric;

use PHPUnit\Framework\TestCase;
use Talentry\Monitoring\Infrastructure\Metric\AwsEmfClient;
use Talentry\Monitoring\Infrastructure\Metric\PhpLeagueStatsdClient;
use Talentry\Monitoring\Infrastructure\Metric\VoidMetricStore;
use Talentry\Monitoring\Infrastructure\Metric\MetricStoreFactory;
use Talentry\Monitoring\Infrastructure\Monitor\StatsdProtocol;

class MetricStoreFactoryTest extends TestCase
{
    public function testWithoutHostAndPort(): void
    {
        $factory = new MetricStoreFactory();
        $store = $factory->generate();

        self::assertInstanceOf(VoidMetricStore::class, $store);
    }

    public function testWithStandardProtocol(): void
    {
        $factory = new MetricStoreFactory('host', 8125, StatsdProtocol::STANDARD->value);
        $store = $factory->generate();

        self::assertInstanceOf(PhpLeagueStatsdClient::class, $store);
    }

    public function testWithAwsEmfProtocol(): void
    {
        $factory = new MetricStoreFactory('host', 8125, StatsdProtocol::AWS_EMF->value);
        $store = $factory->generate();

        self::assertInstanceOf(AwsEmfClient::class, $store);
    }
}
