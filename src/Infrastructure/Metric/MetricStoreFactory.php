<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Metric;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Talentry\ErrorHandling\ErrorHandler;
use Talentry\Monitoring\Domain\Metric\MetricStore;
use Talentry\ErrorHandling\ErrorHandler\NullErrorHandler;
use InvalidArgumentException;
use Talentry\Monitoring\Infrastructure\Monitor\StatsdProtocol;
use Talentry\Monitoring\Infrastructure\Time\TimeProvider;

class MetricStoreFactory
{
    private StatsdProtocol $statsdProtocol;

    public function __construct(
        private readonly ?string $statsdHost = null,
        private readonly ?int $statsdPort = null,
        ?string $statsdProtocol = null,
        private readonly ErrorHandler $errorHandler = new NullErrorHandler(),
        private readonly LoggerInterface $logger = new NullLogger(),
        private readonly TimeProvider $timeProvider = new TimeProvider(),
    ) {
        if ($statsdProtocol !== null) {
            $statsdProtocol = StatsdProtocol::tryFrom($statsdProtocol);
        }
        if ($statsdProtocol === null) {
            $statsdProtocol = StatsdProtocol::default();
        }
        $this->statsdProtocol = $statsdProtocol;
    }

    public function generate(): MetricStore
    {
        if (is_null($this->statsdHost) || is_null($this->statsdPort)) {
            return new VoidMetricStore();
        }

        return match ($this->statsdProtocol) {
            StatsdProtocol::STANDARD => new PhpLeagueStatsdClient(
                $this->statsdHost,
                $this->statsdPort,
                $this->logger,
            ),
            StatsdProtocol::AWS_EMF => new AwsEmfClient(
                $this->statsdHost,
                $this->statsdPort,
                $this->errorHandler,
                $this->logger,
                null,
                $this->timeProvider,
            ),
            default => throw new InvalidArgumentException(
                'Unsupported StatsD protocol: ' . $this->statsdProtocol->value,
            ),
        };
    }
}
