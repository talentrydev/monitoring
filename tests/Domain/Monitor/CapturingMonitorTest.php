<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Domain\Monitor;

use PHPUnit\Framework\TestCase;
use Talentry\Monitoring\Domain\Metric\Metrics;
use Talentry\Monitoring\Domain\Metric\Model\IncrementMetric;
use Talentry\Monitoring\Domain\Monitor\CapturingMonitor;

class CapturingMonitorTest extends TestCase
{
    private CapturingMonitor $capturingMonitor;

    protected function setUp(): void
    {
        $this->capturingMonitor = new CapturingMonitor();
    }

    public function testSupportsAllMetrics(): void
    {
        self::assertTrue($this->capturingMonitor->supports(Metrics::incrementMetric('')));
        self::assertTrue($this->capturingMonitor->supports(Metrics::decrementMetric('')));
        self::assertTrue($this->capturingMonitor->supports(Metrics::gaugeMetric('', 0.0)));
        self::assertTrue($this->capturingMonitor->supports(Metrics::timingMetric('', 0.0)));
    }

    public function testGetCapturedMetrics(): void
    {
        $this->capturingMonitor->push(Metrics::incrementMetric(''));
        $metrics = $this->capturingMonitor->getCapturedMetrics();
        self::assertCount(1, $metrics);
        self::assertInstanceOf(IncrementMetric::class, $metrics[0]);

        $this->capturingMonitor->reset();
        $metrics = $this->capturingMonitor->getCapturedMetrics();
        self::assertCount(0, $metrics);
    }
}
