<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Metric;

use Psr\Log\LoggerInterface;
use Talentry\ErrorHandling\Enum\Severity;
use Talentry\ErrorHandling\ErrorHandler;
use Talentry\Monitoring\Domain\Metric\MetricStore;
use Talentry\Monitoring\Infrastructure\Time\TimeProvider;
use Throwable;
use RuntimeException;

class AwsEmfClient implements MetricStore
{
    private const string DEFAULT_NAMESPACE = 'Production';
    private const string DEFAULT_PROTOCOL = 'udp';
    private const string DEFAULT_LOG_GROUP_NAMESPACE = '/aws/emf/metrics/';
    private const string METRIC_UNIT_MILLISECONDS = 'Milliseconds';

    private int $timeout;

    /**
     * @var resource
     */
    private $socket;

    public function __construct(
        private readonly string $cwAgentHost,
        private readonly int $cwAgentPort,
        private readonly ErrorHandler $errorHandler,
        private readonly LoggerInterface $logger,
        ?int $timeout = null,
        private readonly TimeProvider $timeProvider = new TimeProvider(),
        private readonly string $protocol = self::DEFAULT_PROTOCOL,
        private readonly string $logGroupNamespace = self::DEFAULT_LOG_GROUP_NAMESPACE,
    ) {
        $this->timeout = $timeout ?? (int) ini_get('default_socket_timeout');
    }

    public function increment(
        string $metric,
        float $sampleRate = 1.0,
        array $tags = null,
        ?string $namespace = null,
    ): void {
        $this->sendMetric($metric, 1, null, $tags, $namespace);
    }

    public function decrement(
        string $metric,
        float $sampleRate = 1.0,
        array $tags = null,
        ?string $namespace = null,
    ): void {
        $this->sendMetric($metric, -1, null, $tags, $namespace);
    }

    public function gauge(
        string $metric,
        float $value,
        float $sampleRate = 1.0,
        array $tags = null,
        ?string $namespace = null,
    ): void {
        $this->sendMetric($metric, $value, null, $tags, $namespace);
    }

    public function timing(
        string $metric,
        float $time,
        float $sampleRate = 1.0,
        array $tags = null,
        ?string $namespace = null,
    ): void {
        $this->sendMetric($metric, $time, self::METRIC_UNIT_MILLISECONDS, $tags, $namespace);
    }

    /**
     * @param int|float $metricValue
     * @param array<string,mixed> $tags
     */
    private function sendMetric(
        string $metricName,
        $metricValue,
        ?string $metricUnit = null,
        ?array $tags = null,
        ?string $namespace = null,
    ): void {
        $dimensions = $tags === null ? [] : array_keys($tags);
        $metric = [
            'Name' => $metricName,
        ];
        if ($metricUnit !== null) {
            $metric['Unit'] = $metricUnit;
        }
        $namespace = $namespace ?? self::DEFAULT_NAMESPACE;

        $emfData = [
            '_aws' => [
                'Timestamp' => $this->timeProvider->getCurrentTimeStampInMilliseconds(),
                'LogGroupName' => $this->logGroupNamespace . $namespace,
                'CloudWatchMetrics' => [
                    [
                        'Namespace' => $namespace,
                        'Dimensions' => [$dimensions],
                        'Metrics' => [$metric],
                    ],
                ],
            ],
            $metricName => $metricValue,
        ];


        foreach ($tags ?? [] as $key => $metricValue) {
            $emfData[$key] = $metricValue;
        }

        $this->sendData(json_encode($emfData));
    }

    private function sendData(string $data): void
    {
        $this->errorHandler->startHandling(Severity::WARNING);
        try {
            if (!$this->socket) {
                $this->socket = fsockopen(
                    sprintf('%s://%s', $this->protocol, $this->cwAgentHost),
                    $this->cwAgentPort,
                    $errno,
                    $errstr,
                    $this->timeout,
                );

                if (!$this->socket) {
                    throw new RuntimeException(sprintf('Error creating socket; error %s: %s', $errno, $errstr));
                }
            }

            fwrite($this->socket, $data);
            fflush($this->socket);
        } catch (Throwable $error) {
            $this->logger->error($error->getMessage(), ['trace' => $error->getTraceAsString()]);
        } finally {
            $this->errorHandler->stopHandling();
        }
    }
}
