<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Domain\Metric;

use RuntimeException;
use Talentry\Monitoring\Domain\Metric\Model\Metric;

class UnsupportedMetricException extends RuntimeException
{
    public function __construct(Metric $metric)
    {
        parent::__construct('Unsupported metric: ' . get_class($metric));
    }
}
