<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Domain\Monitor;

use Talentry\Monitoring\Domain\Metric\Model\Metric;

/**
 * Useful for testing only
 */
class CapturingMonitor implements Monitor
{
    private array $capturedMetrics = [];

    public function push(Metric $metric): void
    {
        $this->capturedMetrics[] = $metric;
    }

    public function supports(Metric $metric): bool
    {
        return true;
    }

    public function getCapturedMetrics(): array
    {
        return $this->capturedMetrics;
    }

    public function reset(): void
    {
        $this->capturedMetrics = [];
    }
}
