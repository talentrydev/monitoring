<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Presentation\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Talentry\Monitoring\Infrastructure\Queue\QueuedMetricsProcessor;
use Symfony\Component\Console\Attribute\AsCommand;

#[AsCommand(
    name: 'ty:monitoring:push-queued-metrics',
    description: 'Gets queued metrics and pushes them to metric store',
)]
class PushQueuedMetricsCommand extends Command
{
    public function __construct(
        private readonly QueuedMetricsProcessor $queuedMetricsProcessor,
        private readonly LoggerInterface $logger,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $processed = $this->queuedMetricsProcessor->run();

        $this->logger->debug(sprintf('Processed %s metrics', $processed));

        return self::SUCCESS;
    }
}
