<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Presentation\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Talentry\Monitoring\Domain\Metric\Metrics;
use Talentry\Monitoring\Domain\Monitor\Monitor;
use Symfony\Component\Console\Attribute\AsCommand;

#[AsCommand(name: 'ty:monitoring:push-test-metric', description: 'Pushes a test metric to the monitoring service')]
class PushTestMetricCommand extends Command
{
    public function __construct(
        private readonly Monitor $monitor,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption('name', null, InputOption::VALUE_REQUIRED, '', 'test.metric');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $metric = Metrics::incrementMetric($input->getOption('name'));

        $this->monitor->push($metric);

        return self::SUCCESS;
    }
}
