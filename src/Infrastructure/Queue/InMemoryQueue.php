<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Queue;

use SplQueue;
use Talentry\Monitoring\Domain\Metric\Model\Metric;

class InMemoryQueue implements Queue
{
    public function __construct(
        private readonly SplQueue $queue = new SplQueue(),
    ) {
    }

    public function push(Metric $metric): void
    {
        $this->queue->enqueue($metric);
    }

    public function pop(): ?Metric
    {
        if ($this->queue->isEmpty()) {
            return null;
        }

        return $this->queue->dequeue();
    }
}
