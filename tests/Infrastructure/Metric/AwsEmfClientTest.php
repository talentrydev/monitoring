<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Infrastructure\Metric;

use Psr\Log\NullLogger;
use ColinODell\PsrTestLogger\TestLogger;
use Talentry\Monitoring\Infrastructure\Metric\AwsEmfClient;
use Talentry\Monitoring\Infrastructure\Time\TimeProvider;
use Talentry\Monitoring\Tests\Mock\MockTimeProvider;
use Talentry\Monitoring\Tests\StatsdClientTestCase;

class AwsEmfClientTest extends StatsdClientTestCase
{
    private AwsEmfClient $client;
    private string $defaultNamespace = 'Production';
    private int $currentTimestamp;
    private string $logGroupNamespace = 'aws/test/';

    protected function setUp(): void
    {
        parent::setUp();

        $this->currentTimestamp = (new TimeProvider())->getCurrentTimeStampInMilliseconds();
        $timeProvider = new MockTimeProvider($this->currentTimestamp);
        $this->client = new AwsEmfClient(
            $this->host,
            $this->port,
            $this->errorHandler,
            new NullLogger(),
            null,
            $timeProvider,
            'udp',
            $this->logGroupNamespace,
        );
    }

    public function testIncrementMetric(): void
    {
        $this->client->increment($this->metricName);
        $this->assertIncrementMetricWasPushed();
    }

    public function testIncrementMetricWithNamespace(): void
    {
        $this->client->increment($this->metricName, $this->sampleRate, null, $this->namespace);
        $this->assertIncrementMetricWithNamespaceWasPushed();
    }

    public function testIncrementMetricWithTags(): void
    {
        $this->client->increment($this->metricName, $this->sampleRate, $this->metricTags);
        $this->assertIncrementMetricWithTagsWasPushed();
    }

    public function testDecrementMetric(): void
    {
        $this->client->decrement($this->metricName);
        $this->assertDecrementMetricWasPushed();
    }

    public function testDecrementMetricWithNamespace(): void
    {
        $this->client->decrement($this->metricName, $this->sampleRate, null, $this->namespace);
        $this->assertDecrementMetricWithNamespaceWasPushed();
    }

    public function testDecrementMetricWithTags(): void
    {
        $this->client->decrement($this->metricName, $this->sampleRate, $this->metricTags);
        $this->assertDecrementMetricWithTagsWasPushed();
    }

    public function testGaugeMetric(): void
    {
        $this->client->gauge($this->metricName, $this->metricValue);
        $this->assertGaugeMetricWasPushed();
    }

    public function testGaugeMetricWithNamespace(): void
    {
        $this->client->gauge($this->metricName, $this->metricValue, $this->sampleRate, [], $this->namespace);
        $this->assertGaugeMetricWithNamespaceWasPushed();
    }

    public function testGaugeMetricWithTags(): void
    {
        $this->client->gauge($this->metricName, $this->metricValue, $this->sampleRate, $this->metricTags);
        $this->assertGaugeMetricWithTagsWasPushed();
    }

    public function testTimingMetric(): void
    {
        $this->client->timing($this->metricName, $this->metricValue);
        $this->assertTimingMetricWasPushed();
    }

    public function testTimingMetricWithNamespace(): void
    {
        $this->client->timing($this->metricName, $this->metricValue, $this->sampleRate, [], $this->namespace);
        $this->assertTimingMetricWithNamespaceWasPushed();
    }

    public function testTimingMetricWithTags(): void
    {
        $this->client->timing($this->metricName, $this->metricValue, $this->sampleRate, $this->metricTags);
        $this->assertTimingMetricWithTagsWasPushed();
    }

    public function testWithInvalidHost(): void
    {
        $logger = new TestLogger();
        $client = new AwsEmfClient(
            'foo',
            $this->port,
            $this->errorHandler,
            $logger,
        );
        $client->increment($this->metricName);
        $error = 'Error creating socket; error 0: php_network_getaddresses: getaddrinfo for foo failed:';
        self::assertTrue($logger->hasErrorThatContains($error));
    }

    protected function assertIncrementMetricWasPushed(): void
    {
        $expectedJson = [
            '_aws' => [
                'Timestamp' => $this->currentTimestamp,
                'LogGroupName' => $this->logGroupNamespace . $this->defaultNamespace,
                'CloudWatchMetrics' => [
                    [
                        'Namespace' => $this->defaultNamespace,
                        'Dimensions' => [[]],
                        'Metrics' => [['Name' => $this->metricName]],
                    ],
                ],
            ],
            $this->metricName => 1,
        ];

        self::assertSame(json_encode($expectedJson), $this->readSocket());
    }

    protected function assertIncrementMetricWithNamespaceWasPushed(): void
    {
        $expectedJson = [
            '_aws' => [
                'Timestamp' => $this->currentTimestamp,
                'LogGroupName' => $this->logGroupNamespace . $this->namespace,
                'CloudWatchMetrics' => [
                    [
                        'Namespace' => $this->namespace,
                        'Dimensions' => [[]],
                        'Metrics' => [['Name' => $this->metricName]],
                    ],
                ],
            ],
            $this->metricName => 1,
        ];

        self::assertSame(json_encode($expectedJson), $this->readSocket());
    }

    protected function assertIncrementMetricWithTagsWasPushed(): void
    {
        $expectedJson = [
            '_aws' => [
                'Timestamp' => $this->currentTimestamp,
                'LogGroupName' => $this->logGroupNamespace . $this->defaultNamespace,
                'CloudWatchMetrics' => [
                    [
                        'Namespace' => $this->defaultNamespace,
                        'Dimensions' => [[
                            'foo',
                            'bar',
                        ]],
                        'Metrics' => [['Name' => $this->metricName]],
                    ],
                ],
            ],
            $this->metricName => 1,
            'foo' => $this->metricTags['foo'],
            'bar' => $this->metricTags['bar'],
        ];

        self::assertSame(json_encode($expectedJson), $this->readSocket());
    }

    protected function assertDecrementMetricWasPushed(): void
    {
        $expectedJson = [
            '_aws' => [
                'Timestamp' => $this->currentTimestamp,
                'LogGroupName' => $this->logGroupNamespace . $this->defaultNamespace,
                'CloudWatchMetrics' => [
                    [
                        'Namespace' => $this->defaultNamespace,
                        'Dimensions' => [[]],
                        'Metrics' => [['Name' => $this->metricName]],
                    ],
                ],
            ],
            $this->metricName => -1,
        ];

        self::assertSame(json_encode($expectedJson), $this->readSocket());
    }

    protected function assertDecrementMetricWithNamespaceWasPushed(): void
    {
        $expectedJson = [
            '_aws' => [
                'Timestamp' => $this->currentTimestamp,
                'LogGroupName' => $this->logGroupNamespace . $this->namespace,
                'CloudWatchMetrics' => [
                    [
                        'Namespace' => $this->namespace,
                        'Dimensions' => [[]],
                        'Metrics' => [['Name' => $this->metricName]],
                    ],
                ],
            ],
            $this->metricName => -1,
        ];

        self::assertSame(json_encode($expectedJson), $this->readSocket());
    }

    protected function assertDecrementMetricWithTagsWasPushed(): void
    {
        $expectedJson = [
            '_aws' => [
                'Timestamp' => $this->currentTimestamp,
                'LogGroupName' => $this->logGroupNamespace . $this->defaultNamespace,
                'CloudWatchMetrics' => [
                    [
                        'Namespace' => $this->defaultNamespace,
                        'Dimensions' => [[
                            'foo',
                            'bar',
                        ]],
                        'Metrics' => [['Name' => $this->metricName]],
                    ],
                ],
            ],
            $this->metricName => -1,
            'foo' => $this->metricTags['foo'],
            'bar' => $this->metricTags['bar'],
        ];

        self::assertSame(json_encode($expectedJson), $this->readSocket());
    }

    protected function assertGaugeMetricWasPushed(): void
    {
        $expectedJson = [
            '_aws' => [
                'Timestamp' => $this->currentTimestamp,
                'LogGroupName' => $this->logGroupNamespace . $this->defaultNamespace,
                'CloudWatchMetrics' => [
                    [
                        'Namespace' => $this->defaultNamespace,
                        'Dimensions' => [[]],
                        'Metrics' => [['Name' => $this->metricName]],
                    ],
                ],
            ],
            $this->metricName => $this->metricValue,
        ];

        self::assertSame(json_encode($expectedJson), $this->readSocket());
    }

    protected function assertGaugeMetricWithNamespaceWasPushed(): void
    {
        $expectedJson = [
            '_aws' => [
                'Timestamp' => $this->currentTimestamp,
                'LogGroupName' => $this->logGroupNamespace . $this->namespace,
                'CloudWatchMetrics' => [
                    [
                        'Namespace' => $this->namespace,
                        'Dimensions' => [[]],
                        'Metrics' => [['Name' => $this->metricName]],
                    ],
                ],
            ],
            $this->metricName => $this->metricValue,
        ];

        self::assertSame(json_encode($expectedJson), $this->readSocket());
    }

    protected function assertGaugeMetricWithTagsWasPushed(): void
    {
        $expectedJson = [
            '_aws' => [
                'Timestamp' => $this->currentTimestamp,
                'LogGroupName' => $this->logGroupNamespace . $this->defaultNamespace,
                'CloudWatchMetrics' => [
                    [
                        'Namespace' => $this->defaultNamespace,
                        'Dimensions' => [[
                            'foo',
                            'bar',
                        ]],
                        'Metrics' => [['Name' => $this->metricName]],
                    ],
                ],
            ],
            $this->metricName => $this->metricValue,
            'foo' => $this->metricTags['foo'],
            'bar' => $this->metricTags['bar'],
        ];

        self::assertSame(json_encode($expectedJson), $this->readSocket());
    }

    protected function assertTimingMetricWasPushed(): void
    {
        $expectedJson = [
            '_aws' => [
                'Timestamp' => $this->currentTimestamp,
                'LogGroupName' => $this->logGroupNamespace . $this->defaultNamespace,
                'CloudWatchMetrics' => [
                    [
                        'Namespace' => $this->defaultNamespace,
                        'Dimensions' => [[]],
                        'Metrics' => [[
                            'Name' => $this->metricName,
                            'Unit' => 'Milliseconds',
                        ]],
                    ],
                ],
            ],
            $this->metricName => $this->metricValue,
        ];

        self::assertSame(json_encode($expectedJson), $this->readSocket());
    }

    protected function assertTimingMetricWithNamespaceWasPushed(): void
    {
        $expectedJson = [
            '_aws' => [
                'Timestamp' => $this->currentTimestamp,
                'LogGroupName' => $this->logGroupNamespace . $this->namespace,
                'CloudWatchMetrics' => [
                    [
                        'Namespace' => $this->namespace,
                        'Dimensions' => [[]],
                        'Metrics' => [[
                            'Name' => $this->metricName,
                            'Unit' => 'Milliseconds',
                        ]],
                    ],
                ],
            ],
            $this->metricName => $this->metricValue,
        ];

        self::assertSame(json_encode($expectedJson), $this->readSocket());
    }

    protected function assertTimingMetricWithTagsWasPushed(): void
    {
        $expectedJson = [
            '_aws' => [
                'Timestamp' => $this->currentTimestamp,
                'LogGroupName' => $this->logGroupNamespace . $this->defaultNamespace,
                'CloudWatchMetrics' => [
                    [
                        'Namespace' => $this->defaultNamespace,
                        'Dimensions' => [[
                            'foo',
                            'bar',
                        ]],
                        'Metrics' => [[
                            'Name' => $this->metricName,
                            'Unit' => 'Milliseconds',
                        ]],
                    ],
                ],
            ],
            $this->metricName => $this->metricValue,
            'foo' => $this->metricTags['foo'],
            'bar' => $this->metricTags['bar'],
        ];

        self::assertSame(json_encode($expectedJson), $this->readSocket());
    }
}
