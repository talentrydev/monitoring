<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Domain\Monitor;

use Talentry\Monitoring\Domain\Metric\Model\IncrementMetric;
use Talentry\Monitoring\Domain\Metric\Model\Metric;
use Talentry\Monitoring\Domain\Metric\UnsupportedMetricException;

class IncrementMonitor extends MetricMonitor
{
    public function push(Metric $metric): void
    {
        /** @var IncrementMetric $metric */
        if (!$this->supports($metric)) {
            throw new UnsupportedMetricException($metric);
        }

        $this->getMetricStore()->increment(
            $metric->getName(),
            self::DEFAULT_SAMPLE_RATE,
            $this->getTags($metric),
            $this->namespace,
        );
    }

    public function supports(Metric $metric): bool
    {
        return $metric instanceof IncrementMetric;
    }
}
