<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Domain\Metric\Model;

class MetricWithEnvironment implements Metric
{
    public function __construct(
        private readonly Metric $delegate,
        private readonly string $environment,
    ) {
    }

    public function getName(): string
    {
        return sprintf('%s.%s', $this->environment, $this->delegate->getName());
    }
}
