<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Presentation\Command;

use Psr\Log\NullLogger;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Talentry\Monitoring\Domain\Metric\Metrics;
use Talentry\Monitoring\Domain\Monitor\Monitor;
use Talentry\Monitoring\Infrastructure\Metric\PhpLeagueStatsdClient;
use Talentry\Monitoring\Infrastructure\Monitor\MonitorFactory;
use Talentry\Monitoring\Infrastructure\Queue\InMemoryQueue;
use Talentry\Monitoring\Infrastructure\Queue\QueuedMetricsProcessor;
use Talentry\Monitoring\Presentation\Command\PushQueuedMetricsCommand;
use Talentry\Monitoring\Tests\StatsdClientTestCase;

class PushQueuedMetricsCommandTest extends StatsdClientTestCase
{
    private Monitor $monitor;
    private PushQueuedMetricsCommand $command;

    protected function setUp(): void
    {
        parent::setUp();

        $metricStore = new PhpLeagueStatsdClient($this->host, $this->port);
        $this->monitor = (new MonitorFactory($metricStore, new InMemoryQueue()))->generate();
        $this->command = new PushQueuedMetricsCommand(new QueuedMetricsProcessor($this->monitor), new NullLogger());
    }

    public function testCommand(): void
    {
        $this->monitor->push(Metrics::incrementMetric($this->metricName));
        $this->assertNoMetricsWerePushed();

        $return = $this->command->run(new ArrayInput([]), new NullOutput());
        self::assertSame(0, $return);
        $this->assertIncrementMetricWasPushed();
    }
}
